package com.data.mall.service;

import com.data.mall.domain.Item;
import com.data.mall.exception.BusinessException;
import com.data.mall.service.dto.ItemDTO;

import java.util.List;
import java.util.function.LongFunction;

/**
 * Created by hzllb on 2018/11/18.
 */
public interface ItemService {

    /**
     * 商品创建
     * @param itemDTO
     * @return
     * @throws BusinessException
     */
    ItemDTO createItem(ItemDTO itemDTO) throws BusinessException;

    /**
     * 商品列表浏览
     * @return
     */
    List<ItemDTO> listItem();

    //商品详情浏览
    ItemDTO getItemById(Long id);

    //item及promo model缓存模型
    ItemDTO getItemByIdInCache(Long id);

    //库存扣减
    boolean decreaseStock(Long itemId, Integer amount)throws BusinessException;
    //库存回补
    boolean increaseStock(Long itemId, Integer amount)throws BusinessException;

    //商品销量增加
    void increaseSales(Long itemId, Integer amount)throws BusinessException;

    //初始化库存流水
    String initStockLog(Long itemId, Integer amount);



}
