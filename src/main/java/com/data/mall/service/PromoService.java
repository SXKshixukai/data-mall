package com.data.mall.service;

import com.data.mall.service.dto.PromoDTO;

/**
 * Created by hzllb on 2018/11/18.
 */
public interface PromoService {
    /**
     * 根据itemid获取即将进行的或正在进行的秒杀活动
     * @param itemId
     * @return
     */
    PromoDTO getPromoByItemId(Long itemId);

    /**
     * 活动发布
     * @param promoId
     */
    void publishPromo(Long promoId);

    /**
     * 生成秒杀用的令牌
     * @param promoId
     * @param itemId
     * @param userId
     * @return
     */
    String generateSecondKillToken(Long promoId, Long itemId, Long userId);
}
