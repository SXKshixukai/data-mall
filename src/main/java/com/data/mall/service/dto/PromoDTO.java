package com.data.mall.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author shixukai
 * @Package com.data.mall.service.dto
 * @Description: TODO
 * @date 2021/4/6
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PromoDTO {
    private Long id;
    /**
     * 秒杀活动状态 1表示还未开始，2表示进行中，3表示已结束
     */
    private Integer status;
    /**
     * 秒杀活动名称
     */
    private String promoName;
    /**
     * 秒杀活动开始时间
     */
    private LocalDateTime startTime;
    /**
     * 秒杀活动结束时间
     */
    private LocalDateTime endTime;
    /**
     * 秒杀活动适用商品
     */
    private Integer itemId;
    /**
     * 秒杀活动商品价格
     */
    private String promoItemPrice;
}
