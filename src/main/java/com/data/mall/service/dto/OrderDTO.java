package com.data.mall.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shixukai
 * @Package com.data.mall.service.dto
 * @Description: TODO
 * @date 2021/4/6
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {

    private Long id;
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 购买用户的id
     */
    private Long userId;
    /**
     * 购买的商品id
     */
    private Long itemId;
    /**
     * 活动id，若非空，则表示以秒杀方式下单
     */
    private Long promoId;
    /**
     * 购买商品的单价,若promoId非空，则表示秒杀商品价格
     */
    private String itemPrice;
    /**
     * 购买数量
     */
    private Integer amount;
    /**
     * 购买金额,若promoId非空，则表示秒杀商品价格
     */
    private String orderPrice;
}