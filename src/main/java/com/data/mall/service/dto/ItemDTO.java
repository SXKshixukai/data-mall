package com.data.mall.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author shixukai
 * @Package com.data.mall.service.dto
 * @Description: TODO
 * @date 2021/4/6
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemDTO {

    private Long id;
    /**
     * 商品名称
     */
    private String title;
    /**
     * 商品价格
     */
    private String price;
    /**
     * 商品的库存
     */
    private Integer stock;
    /**
     * 商品的描述
     */
    private String description;
    /**
     * 商品的销量
     */
    private Integer sales;
    /**
     * 商品描述图片的url
     */
    private String imgUrl;
    /**
     * 使用聚合模型，如果promoDto不为空，则表示其拥有还未结束的秒杀活动
     */
    private PromoDTO promoDTO;

}
