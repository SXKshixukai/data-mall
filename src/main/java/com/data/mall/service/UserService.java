package com.data.mall.service;

import com.data.mall.exception.BusinessException;
import com.data.mall.service.dto.UserDTO;

/**
 * Created by hzllb on 2018/11/11.
 */
public interface UserService {
    /**
     * 通过用户ID获取用户对象的方法
     * @param id
     * @return
     */
    UserDTO getUserById(Long id);

    /**
     * 通过缓存获取用户对象
     * @param id
     * @return
     */
    UserDTO getUserByIdInCache(Long id);

    void register(UserDTO userModel) throws BusinessException;

    /**
     * telphone:用户注册手机
     * password:用户加密后的密码
     */
    UserDTO validateLogin(String telphone, String encrptPassword) throws BusinessException;
}
