package com.data.mall.data.dto;

/**
 * 最受还原的商品数据展示
 */
public class FavorItemVO {
    private String title;

    private Double price;

    private String description;

    private Integer sales;

    private String imgUrl;

    /**
     * 浏览次数
     */
    private Integer broseMount;
}
