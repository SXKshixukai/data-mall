package com.data.mall.data.service;

import com.data.mall.data.dto.FavorItemVO;
import com.data.mall.service.dto.ItemDTO;
import com.data.mall.service.dto.UserDTO;

import java.time.LocalDateTime;
import java.util.List;

public interface DataCollectService {

    /**
     * 商品详情浏览记录埋点
     */
    void goodsDetailRecord(ItemDTO item, UserDTO user);

    /**
     * 获取商详浏览数据
     */
    List<FavorItemVO> obtainFavorItemData(LocalDateTime startTime, LocalDateTime endTime);
}
