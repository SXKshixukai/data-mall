package com.data.mall.data.service.impl;

import com.data.mall.common.KafkaTrackingDTO;
import com.data.mall.data.dto.FavorItemVO;
import com.data.mall.data.service.DataCollectService;
import com.data.mall.etl.domain.enums.Event;
import com.data.mall.etl.domain.enums.Params;
import com.data.mall.kafka.KafkaProduceService;
import com.data.mall.service.dto.ItemDTO;
import com.data.mall.service.dto.UserDTO;
import com.data.mall.utils.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Service
public class DateCollectServiceImpl implements DataCollectService {

    @Autowired
    KafkaProduceService kafkaProduceService;

    @Override
    public void goodsDetailRecord(ItemDTO item, UserDTO user) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        KafkaTrackingDTO kafkaTrackingDTO = KafkaTrackingDTO.builder()
                .event(Event.USER_VIEW)
                .param(Params.USER_ID, user.getId().toString())
                .param(Params.ITEM_ID, item.getId().toString())
                .param(Params.TIME, LocalDateTime.now().format(formatter))
                .build();
        // 活动信息埋点
        if (Objects.nonNull(item.getPromoDTO())) {
            kafkaTrackingDTO.getParamList().put(Params.PROMOTION_ID, item.getPromoDTO().getId().toString());
        }
        kafkaProduceService.sendTrackingMessage(kafkaTrackingDTO);
    }

    @Override
    public List<FavorItemVO> obtainFavorItemData(LocalDateTime startTime, LocalDateTime endTime) {
        return null;
    }
}
