package com.data.mall.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author YQSL
 * @data 2020/9/24
 * @description 响应状态码
 */
@AllArgsConstructor
@Getter
public enum ResponseCodeEnum {

    // 0 成功
    SUCCESS(0, "操作成功"),

    // 1*** 参数异常
    PARAM_ERROR(100001, "参数异常"),

    // 2*** 系统异常
    SYSTEM_ERROR(200001, "服务异常"),
    UNKNOWN_ERROR(200002, "未知异常"),

    // 3*** 业务异常
    RATE_LIMIT_ERROR(300001, "最大限流异常"),
    FILE_UPLOAD_FAILURE(300002, "文件上传失败"),

    // 4*** 具体业务异常 4(具体业务异常标志位) 001(具体业务分类) 01(具体异常说明)
    PARAMETER_VALIDATION_ERROR(400001,"业务异常")
    ;

    /**
     * 编码
     */
    private Integer code;

    /**
     * 含义
     */
    private String message;
}
