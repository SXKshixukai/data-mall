package com.data.mall.exception;

import com.data.mall.utils.http.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author shixukai
 * @Package com.yq.weixin.exception
 * @Description: TODO
 * @date 2020/12/10
 */
@Slf4j
@Order(0)
@ControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(RuntimeException.class)
    public @ResponseBody
    Response runtimeExceptionHandler(RuntimeException ex) {
        log.error("RuntimeException: ", ex);
        return Response.getInstance(ResponseCodeEnum.SYSTEM_ERROR.getCode(),
                ex.getMessage());
    }

    @ExceptionHandler(value = BusinessException.class)
    public @ResponseBody
    Response businessExceptionHandle(BusinessException e) {
        log.info("捕捉到业务类异常：", e);
        return Response.getInstance(e.getCode(),
                e.getMessage());
    }
}
