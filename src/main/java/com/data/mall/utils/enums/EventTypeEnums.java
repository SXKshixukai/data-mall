package com.data.mall.utils.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author shixukai
 * @Package com.data.mall.utils.enums
 * @Description: TODO
 * @date 2021/4/29
 */
@AllArgsConstructor
@Getter
public enum EventTypeEnums {
    ANY("ANY","其他消息"),
    USER_VIEW("USER_VIEW","用户浏览");

    private String event;
    private String name;

    public static EventTypeEnums of(String aType) {
        for (EventTypeEnums memberEvent : values()) {
            if (memberEvent.name().equals(aType)) {
                return memberEvent;
            }
        }
        return ANY;
    }
}
