package com.data.mall.utils;

import com.data.mall.config.JsonConfiguration;
import lombok.SneakyThrows;

public class JSON {
    @SneakyThrows
    public static String toJSONString(Object v) {
        return JsonConfiguration.getObjectMapper().writeValueAsString(v);
    }

    @SneakyThrows
    public static <T> T parseObject(String v, Class<T> tClass) {
        return JsonConfiguration.getObjectMapper().readValue(v, tClass);
    }
}
