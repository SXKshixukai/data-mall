package com.data.mall.utils;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class CommonUtil {
    public static String trim(String value, String str) {
        int len = value.length();

//        log.info("str: {}",value.substring(0, 1));
        if (value.substring(0, 1).equals(str)) {
//            log.info("1");
            value = value.substring(1, len - 1);
            len = value.length();
        }
        if (value.substring(len - 1, len).equals(str)) {
            value = value.substring(0, len - 1);
        }
        return value;
    }

    public static String[] splitByFirst(String content, String key) {
        StringJoiner keyWords = new StringJoiner("");
        String sourceCode = "";

        for (int i=0;i < content.length(); i++) {
            String c = String.valueOf(content.charAt(i));
            if (!c.equals(key)) {
                keyWords.add(c);
            }
            if (c.equals(key)) {
                sourceCode = content.substring(i + 1);
                break;
            }
        }
        return new String[]{keyWords.toString(), sourceCode};
    }

    /**
     *
     */

    /**
     * 生成16位不重复的随机数，含数字+大小写
     *
     * @return
     */
    public static String getRandom(Integer num) {
        if (num < 2) {
            return "";
        }
        StringBuilder uid = new StringBuilder();
        //产生16位的强随机数
        Random rd = new SecureRandom();
        for (int i = 0; i < num; i++) {
            //产生0-2的3位随机数
            int type = rd.nextInt(2);
            switch (type) {
                case 0:
                    //0-9的随机数
                    uid.append(rd.nextInt(10));
                    break;
                case 1:
                    //ASCII在65-90之间为大写,获取大写随机
                    uid.append((char) (rd.nextInt(25) + 65));
                    break;

                default:
                    break;
            }
        }
        return uid.toString();
    }

    /**
     * @Description list 随机取数据
     * @params     list    list集合
     *           num     随机取多少条
     *
     * @return*/
    public static Object getRandomList(List list) {
        if (list.size() <= 1) {
            return list.get(0);
        } else {
            Random random = new Random();
            int size = list.size();
            int intRandom = random.nextInt(size);
            return list.get(intRandom);
        }
    }

    /**
     * 替换字符串占位符, 字符串中使用{key}表示占位符
     * <p>
     * 利用反射 自动获取对象属性值 (必须有get方法)
     *
     * @param sourceString 需要匹配的字符串
     * @param param        参数集
     * @return
     */
    private static final Pattern pattern = Pattern.compile("\\{(.*?)\\}");
    private static Matcher matcher;
    public static String replaceWithObject(String sourceString, Object param) {
        if (Strings.isNullOrEmpty(sourceString) || ObjectUtils.isEmpty(param)) {
            return sourceString;
        }

        String targetString = sourceString;

        PropertyDescriptor pd;
        Method getMethod;

        // 匹配{}中间的内容 包括括号
        matcher = pattern.matcher(sourceString);
        while (matcher.find()) {
            String key = matcher.group();
            String holderName = key.substring(1, key.length() - 1).trim();
            try {
                pd = new PropertyDescriptor(holderName, param.getClass());
                getMethod = pd.getReadMethod(); // 获得get方法
                Object value = getMethod.invoke(param);
                if (value != null) {
                    targetString = targetString.replace(key, value.toString());
                }
            } catch (Exception e) {
                throw new RuntimeException("String formatter failed", e);
            }
        }
        return targetString;
    }
}