package com.data.mall.common;

import com.data.mall.etl.domain.enums.Event;
import com.data.mall.etl.domain.enums.Params;
import com.data.mall.utils.enums.EventTypeEnums;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author shixukai
 * @Package com.data.mall.common.dto
 * @Description: TODO
 * @date 2021/4/29
 */
@Data
public class KafkaTrackingDTO {
    /**
     * 事件类型
     */
    private Event event;
    /**
     * 时间参数列表
     */
    private Map<Params,String> paramList;

    /**
     * 强制不能通过无参方式创建
     */
    private KafkaTrackingDTO(){

    }

    public static KafkaTrackingBuilder builder() {
        KafkaTrackingBuilder builder = new KafkaTrackingBuilder();
        return builder;
    }

    public static class KafkaTrackingBuilder{
        private Event event;
        private Map<Params,String> paramList;
        /**
         * 提供调用入口
         */
        public KafkaTrackingDTO build() {
            KafkaTrackingDTO kafkaTrackingDTO = new KafkaTrackingDTO();
            kafkaTrackingDTO.setEvent(this.event);
            kafkaTrackingDTO.setParamList(this.paramList);
            return kafkaTrackingDTO;
        }

        public KafkaTrackingBuilder(){
            this.paramList = new HashMap<>();
        }

        public KafkaTrackingBuilder event(Event event){
            this.event = event;
            return this;
        }

        public KafkaTrackingBuilder param(Params key,String param){
            this.paramList.put(key,param);
            return this;
        }
    }
}
