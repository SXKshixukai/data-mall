package com.data.mall.kafka;

import com.data.mall.common.KafkaTrackingDTO;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface KafkaProduceService {
    /**
     * 同步库存扣减消息
     * @param itemId
     * @param amount
     */
    boolean transactionAsyncReduceStock(Long userId, Long itemId, Long promoId, Integer amount, String stockLogId);

    /**
     * 发送埋点消息，不保证重发
     * @param kafkaTrackingDTO kafka消息发送实体
     */
    void sendTrackingMessage(KafkaTrackingDTO kafkaTrackingDTO);
}
