package com.data.mall.kafka.controller;

import com.data.mall.common.KafkaTrackingDTO;
import com.data.mall.kafka.KafkaProduceService;
import com.data.mall.utils.http.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shixukai
 * @Package com.data.mall.kafka.controller
 * @Description: TODO
 * @date 2021/5/2
 */
@RestController
public class KafkaProduceController {
    @Autowired
    private KafkaProduceService kafkaProduceService;

    @PostMapping("/kafka/traking")
    public Response sendTrackingMessage(@RequestBody KafkaTrackingDTO object){
        kafkaProduceService.sendTrackingMessage(object);
        return Response.ok();
    }
}
