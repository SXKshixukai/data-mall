package com.data.mall.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.yandex.clickhouse.ClickHouseConnection;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
@Component
public class ClickHouseConfig {
    @Value("${clickhouse.address}")
    private String ckAddress;
    @Value("${clickhouse.username}")
    private String ckUserName;
    @Value("${clickhouse.password}")
    private String ckPassword;
    @Value("${clickhouse.db}")
    private String ckDb;
    @Value("${clickhouse.socketTimeout}")
    private int ckSocketTimeout;

    public Connection getConn() {
        ClickHouseConnection conn = null;
        ClickHouseProperties properties = new ClickHouseProperties();
        properties.setUser(ckUserName);
        properties.setPassword(ckPassword);
        properties.setDatabase(ckDb);
        properties.setSocketTimeout(ckSocketTimeout);
        ClickHouseDataSource clickHouseDataSource = new ClickHouseDataSource(ckAddress,properties);
        try {
            conn = clickHouseDataSource.getConnection();
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
