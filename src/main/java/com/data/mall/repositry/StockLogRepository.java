package com.data.mall.repositry;

import com.data.mall.domain.StockLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockLogRepository extends JpaRepository<StockLog,Long> {

    StockLog findByStockLogId(String stockLogId);
}
