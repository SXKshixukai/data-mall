package com.data.mall.repositry;

import com.data.mall.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author shixukai
 * @Package com.data.mall.repositry
 * @Description: TODO
 * @date 2021/4/6
 */
public interface UserRepository extends JpaRepository<User,Long> {
    /**
     * 根据手机号查询用户信息
     * @param telphone 用户手机号
     * @return 用户信息
     */
    User findByTelphone(String telphone);
}
