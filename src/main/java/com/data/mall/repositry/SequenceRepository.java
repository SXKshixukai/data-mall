package com.data.mall.repositry;

import com.data.mall.domain.Sequence;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author shixukai
 * @Package com.data.mall.repositry
 * @Description: TODO
 * @date 2021/4/6
 */
public interface SequenceRepository extends JpaRepository<Sequence,Long> {

    Sequence findByName(String name);
}
