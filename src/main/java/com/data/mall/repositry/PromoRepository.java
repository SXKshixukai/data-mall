package com.data.mall.repositry;

import com.data.mall.domain.Promo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromoRepository extends JpaRepository<Promo,Long> {

    Promo findByItemId(Long itemId);
}
