package com.data.mall.repositry;

import com.data.mall.domain.UserPassword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPasswordRepository extends JpaRepository<UserPassword,Long> {

    UserPassword findByUserId(Long userId);
}
