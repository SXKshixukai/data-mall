package com.data.mall.repositry;

import com.data.mall.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ItemRepository extends JpaRepository<Item,Long> {

    @Transactional
    @Modifying
    @Query("update Item o set o.sales = o.sales+:amount where o.id = :itemId ")
    void increaseSales(Long itemId,Integer amount);

}
