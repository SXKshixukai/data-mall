package com.data.mall.repositry;

import com.data.mall.domain.ItemStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ItemStockRepository extends JpaRepository<ItemStock,Long> {

    ItemStock findByItemId(Long itemId);
    @Query("update ItemStock o set o.stock=o.stock-:amount where o.itemId = :itemId")
    void decreaseStock(@Param("itemId") Long itemId,@Param("amount") Integer amount);

}
