package com.data.mall.etl.service.impl;

import com.data.mall.common.KafkaTrackingDTO;
import com.data.mall.etl.domain.enums.Params;
import com.data.mall.etl.service.ItemEventETL;
import com.data.mall.service.dto.ItemDTO;
import com.data.mall.utils.CKUtils;
import com.data.mall.utils.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Map;

@Slf4j
@Service
public class ItemEventETLImpl implements ItemEventETL {

    @Autowired
    private CKUtils ckUtils;

    @Override
    public Object createItem(KafkaTrackingDTO dto) {
        // 商品详情
        insertItem(dto);
        return dto;
    }

    private void insertItem(KafkaTrackingDTO dto) {
        Map<Params, String> param = dto.getParamList();
        ItemDTO item = JSON.parseObject(param.get(Params.ITEM), ItemDTO.class);
        String sql = "insert into datamall.dim_item(item_id,title,price,description) VALUES (" +
                item.getId() + ",'" + item.getTitle() + "','" + item.getPrice() + "','" + item.getDescription() + "'" +
                ")";
        try {
            ckUtils.insert(sql);
        } catch (SQLException e) {
            log.error("insertMemberInfo error");
        }
    }
}
