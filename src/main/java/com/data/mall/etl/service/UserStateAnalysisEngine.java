package com.data.mall.etl.service;

import com.data.mall.etl.dto.EventStateAnalysisDTO;

/**
 * @author shixukai
 * @Package com.data.mall.etl.service
 * @Description: 用户行为分析引擎
 * @date 2021/5/7
 */
public interface UserStateAnalysisEngine {
    /**
     * 用户事件分析
     * @param dto 用户事件分析实体
     */
    void eventAnalysis(EventStateAnalysisDTO dto);

    /**
     * 漏斗分析
     * 根据漏斗实体的
     */

}
