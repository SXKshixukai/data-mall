package com.data.mall.etl.service;

import com.data.mall.etl.dto.FunnelAnalysisDTO;

import java.util.List;

/**
 * @author shixukai
 * @Package com.data.mall.etl.service
 * @Description: TODO
 * @date 2021/5/4
 */
public interface FunnelAnalysisService {
    /**
     * 保存漏斗
     * @param dto
     */
    void save(FunnelAnalysisDTO dto);

    /**
     * 根据id查询漏斗信息
     * @param id
     * @return
     */
    FunnelAnalysisDTO findByFunnelId(Long id);

    /**
     * 查询所有用户信息
     * @return
     */
    List<FunnelAnalysisDTO> findByFunnelList();
}
