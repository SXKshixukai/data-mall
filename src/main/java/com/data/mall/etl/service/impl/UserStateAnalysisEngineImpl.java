package com.data.mall.etl.service.impl;

import com.data.mall.etl.dto.EventStateAnalysisDTO;
import com.data.mall.etl.service.UserStateAnalysisEngine;
import com.data.mall.utils.CKUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author shixukai
 * @Package com.data.mall.etl.service.impl
 * @Description: TODO
 * @date 2021/5/7
 */
public class UserStateAnalysisEngineImpl implements UserStateAnalysisEngine {

    @Autowired
    private CKUtils ckUtils;

    /**
     * 用户事件分析的定义为用户该事件过去一段时间的
     * 折线图
     * @param dto 用户事件分析实体
     */
    @Override
    public void eventAnalysis(EventStateAnalysisDTO dto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String sql = "select count(*),toDate(time) from datamall."+dto.getEvent().getTable()+
                "where time between "+ dto.getStartTime().format(formatter)+" and "+dto.getEndTime().format(formatter)+" "+
                "group by toDate(time) ";
    }


}
