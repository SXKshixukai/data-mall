package com.data.mall.etl.service;

import com.data.mall.common.KafkaTrackingDTO;

/**
 * 用户事件埋点etl
 */
public interface UserEventETL {

    Object userViewEvent(KafkaTrackingDTO dto);

    Object userOrderEvent(KafkaTrackingDTO dto);

    Object userLoginEvent(KafkaTrackingDTO dto);

    Object userReg(KafkaTrackingDTO dto);
}
