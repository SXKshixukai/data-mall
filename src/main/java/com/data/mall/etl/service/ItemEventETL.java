package com.data.mall.etl.service;

import com.data.mall.common.KafkaTrackingDTO;

public interface ItemEventETL {

    Object createItem(KafkaTrackingDTO dto);
}
