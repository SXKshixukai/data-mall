package com.data.mall.etl.service;

import com.data.mall.common.KafkaTrackingDTO;
import com.data.mall.etl.domain.enums.Event;
import com.data.mall.etl.dto.EventParamsDTO;

/**
 * @author shixukai
 * @Package com.data.mall.etl.service
 * @Description: TODO
 * @date 2021/5/3
 */
public interface EventParamsService {
    /**
     * 保存事件参数信息
     * @param dto 事件参数信息
     */
    void save(EventParamsDTO dto);

    /**
     * 根据事件寻找事件参数
     * @param event 事件名称
     * @return 事件参数实体
     */
    EventParamsDTO selectByEvent(Event event);
}
