package com.data.mall.etl.service;

import com.data.mall.common.KafkaTrackingDTO;

public interface PromotionEventETL {
    Object publishPromotion(KafkaTrackingDTO dto);
}
