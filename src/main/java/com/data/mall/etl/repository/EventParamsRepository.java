package com.data.mall.etl.repository;

import com.data.mall.etl.domain.EventParams;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author shixukai
 * @Package com.data.mall.etl.repository
 * @Description: TODO
 * @date 2021/5/3
 */
public interface EventParamsRepository extends JpaRepository<EventParams,Long> {

    int countAllByEvent(String event);

    List<EventParams> findAllByEvent(String event);

}
