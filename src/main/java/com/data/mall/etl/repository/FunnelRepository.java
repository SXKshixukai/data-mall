package com.data.mall.etl.repository;

import com.data.mall.etl.domain.Funnel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author shixukai
 * @Package com.data.mall.etl.repository
 * @Description: TODO
 * @date 2021/5/4
 */
public interface FunnelRepository extends JpaRepository<Funnel,Long> {
}
