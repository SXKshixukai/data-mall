package com.data.mall.etl.repository;

import com.data.mall.etl.domain.FunnelAnalysis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author shixukai
 * @Package com.data.mall.etl.repository
 * @Description: TODO
 * @date 2021/5/4
 */
public interface FunnelAnalysisRepository extends JpaRepository<FunnelAnalysis,Long> {
    /**
     * 根据漏斗Id查询漏斗信息
     * @param funnelId 漏斗id
     * @return 返回漏斗链
     */
    List<FunnelAnalysis> findAllByFunnelIdOrderByOrderSort(Long funnelId);
}
