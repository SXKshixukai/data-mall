package com.data.mall.etl.dto;

import com.data.mall.etl.domain.enums.Params;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shixukai
 * @Package com.data.mall.etl.dto
 * @Description: TODO
 * @date 2021/5/4
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParamsDTO {
    private Params params;
    private String paramValue;
}
