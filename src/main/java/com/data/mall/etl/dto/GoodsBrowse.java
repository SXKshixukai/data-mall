package com.data.mall.etl.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 商详浏览记录
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsBrowse {

    /**
     * 商品ID
     */
    private Long itemId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 浏览时间
     */
    private String browseTime;

}
