package com.data.mall.etl.dto;

import com.data.mall.etl.domain.enums.Event;
import com.data.mall.etl.domain.enums.Params;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author shixukai
 * @Package com.data.mall.etl.dto
 * @Description: TODO
 * @date 2021/5/7
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EventStateAnalysisDTO {
    /**
     * 事件信息
     */
    private Event Event;
    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 事件结束时间
     */
    private LocalDateTime endTime;

}
