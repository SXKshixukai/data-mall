package com.data.mall.etl.dto;

import com.data.mall.etl.domain.enums.Event;
import com.data.mall.etl.domain.enums.Params;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author shixukai
 * @Package com.data.mall.etl.dto
 * @Description: TODO
 * @date 2021/5/3
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EventParamsDTO {
    /**
     * 事件类型
     */
    private Event event;
    /**
     * 时间参数列表
     */
    private List<Params> paramList;
}
