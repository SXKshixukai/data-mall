package com.data.mall.etl.dto;

import com.data.mall.etl.domain.FunnelAnalysis;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author shixukai
 * @Package com.data.mall.etl.dto
 * @Description: TODO
 * @date 2021/5/4
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FunnelAnalysisDTO {
    private String funnelName;
    private List<FunnelAnalysis> funnelAnalysisList;
}
