package com.data.mall.etl.domain;

import com.data.mall.common.BaseEntity;
import com.fasterxml.jackson.databind.ser.Serializers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author shixukai
 * @Package com.data.mall.etl.domain
 * @Description: TODO
 * @date 2021/5/4
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_funnel")
public class Funnel extends BaseEntity {
    private String funnelName;
}
