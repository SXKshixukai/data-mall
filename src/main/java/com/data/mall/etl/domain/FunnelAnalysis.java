package com.data.mall.etl.domain;

import com.data.mall.common.BaseEntity;
import com.data.mall.etl.domain.enums.Event;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author shixukai
 * @Package com.data.mall.etl.domain
 * @Description: 漏斗分析实体(漏斗信息表与事件的关联表)
 * @date 2021/5/4
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_funnel_event")
public class FunnelAnalysis extends BaseEntity {
    /**
     * 根节点id
     * 根节点为 0
     */
    private Long funnelId;
    /**
     * 节点事件
     */
    private Event event;
    /**
     * 事件排序
     */
    private Integer orderSort;
}
