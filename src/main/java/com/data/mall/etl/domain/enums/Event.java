package com.data.mall.etl.domain.enums;

import com.data.mall.utils.enums.EventTypeEnums;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Event {
    ANY("ANY","其他消息",null),

    USER_VIEW("USER_VIEW","用户浏览","user_view"),
    USER_SIGN("USER_SIGN","用户登录","user_login"),
    USER_ORDER("USER_ORDER", "用户下单","user_order"),

    USER_REG("USER_REG", "用户注册","dim_user"),
    CREATE_ITEM("USER_VIEW", "商品创建","dim_item"),
    PUBLISH_PROMOTION("PUBLISH_PROMOTION", "活动上线","dim_promotion")


    ;
    private String event;
    private String name;
    private String table;

    public static Event of(String aType) {
        for (Event memberEvent : values()) {
            if (memberEvent.name().equals(aType)) {
                return memberEvent;
            }
        }
        return ANY;
    }
}
