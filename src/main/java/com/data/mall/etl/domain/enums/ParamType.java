package com.data.mall.etl.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ParamType {

    String("字符串", "String"), Number("数字", "Number"), Json("json格式", "json格式");

    private String name;
    private String type;
}