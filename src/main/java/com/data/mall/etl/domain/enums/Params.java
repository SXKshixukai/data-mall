package com.data.mall.etl.domain.enums;

import com.data.mall.service.dto.ItemDTO;
import com.data.mall.service.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.omg.CORBA.Any;

/**
 * @author shixukai
 * @Package com.data.mall.etl.domain.enums
 * @Description: TODO
 * @date 2021/5/2
 */
@Getter
@AllArgsConstructor
public enum Params {
    USER("USER", "用户信息", ParamType.Json),
    ITEM("ITEM", "用户信息", ParamType.Json),
    PROMOTION("PROMOTION", "活动信息", ParamType.Json),
    ORDER("ORDER", "订单", ParamType.Json),

    USER_ID("USER_ID", "用户ID", ParamType.Number),
    ITEM_ID("ITEM_ID", "商品ID", ParamType.Number),
    TIME("TIME", "产生时间", ParamType.String),
    PROMOTION_ID("PROMOTION_ID", "活动ID", ParamType.Number),
    ANY("ANY","其他属性",ParamType.String),
    ;
    private String params;
    private String name;
    private ParamType paramType;

    public static Params of(String aType) {
        for (Params params : values()) {
            if (params.name().equals(aType)) {
                return params;
            }
        }
        return ANY;
    }
}
