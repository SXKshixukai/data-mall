package com.data.mall.etl.domain;

import com.data.mall.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_event_params")
public class EventParams extends BaseEntity {

    @Column(columnDefinition = "varchar(50) COMMENT '事件英文名称'")
    private String event;
    @Column(columnDefinition = "varchar(50) COMMENT '事件中文名称'")
    private String eventName;
    @Column(columnDefinition = "varchar(50) COMMENT '参数英文名称'")
    private String name;
    @Column(columnDefinition = "varchar(50) COMMENT '参数中文名称'")
    private String paramName;

}