package com.data.mall.domain;

import com.data.mall.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author shixukai
 * @Package com.data.mall.domain
 * @Description: TODO
 * @date 2021/4/5
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_stock_log")
public class StockLog extends BaseEntity {
    private String stockLogId;

    private Long itemId;

    private Integer amount;

    private Integer status;
}
