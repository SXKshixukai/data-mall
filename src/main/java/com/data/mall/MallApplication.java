package com.data.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author shixukai
 * @Package com.data.mall
 * @Description: TODO
 * @date 2021/4/5
 */
@EnableAsync
@SpringBootApplication(scanBasePackages = {"com.data.mall"})
public class MallApplication {

    public static void main(String[] args) {
        System.out.println("start <<<<");
        SpringApplication.run(MallApplication.class, args);
        System.out.println("end >>>");
    }

}
