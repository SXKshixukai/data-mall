package com.data.mall;

import com.data.mall.config.ClickHouseConfig;
import com.data.mall.utils.CKUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableCaching
public class TestConfiguration {

    @Autowired
    ClickHouseConfig clickHouseConfig;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    CKUtils ckUtils;

    @Test
    public void testClickHouse() {
        System.out.println(clickHouseConfig.getConn().toString());
    }

    @Test
    public void testRedis() {
        redisTemplate.opsForValue().set("aaa", 521);
        System.out.println("===========================");
        System.out.println(redisTemplate.opsForValue().get("aaa"));
        System.out.println("===========================");
    }

    @Test
    public void testCkUtils() throws SQLException {
        String sql = "insert into datamall.broseItem (item_id, user_id, browse_time) VALUES (102, 21, 'asdad')";
        ckUtils.insert(sql);
    }
}
